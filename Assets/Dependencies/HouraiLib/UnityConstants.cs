// This file is auto-generated. Modifications are not saved.

namespace UnityConstants
{
    public static class Tags
    {
        /// <summary>
        /// Name of tag 'Untagged'.
        /// </summary>
        public const string Untagged = "Untagged";
        /// <summary>
        /// Name of tag 'Respawn'.
        /// </summary>
        public const string Respawn = "Respawn";
        /// <summary>
        /// Name of tag 'Finish'.
        /// </summary>
        public const string Finish = "Finish";
        /// <summary>
        /// Name of tag 'EditorOnly'.
        /// </summary>
        public const string EditorOnly = "EditorOnly";
        /// <summary>
        /// Name of tag 'MainCamera'.
        /// </summary>
        public const string MainCamera = "MainCamera";
        /// <summary>
        /// Name of tag 'Player'.
        /// </summary>
        public const string Player = "Player";
        /// <summary>
        /// Name of tag 'GameController'.
        /// </summary>
        public const string GameController = "GameController";
        /// <summary>
        /// Name of tag 'Platform'.
        /// </summary>
        public const string Platform = "Platform";
        /// <summary>
        /// Name of tag 'Spawn'.
        /// </summary>
        public const string Spawn = "Spawn";
        /// <summary>
        /// Name of tag 'GUI'.
        /// </summary>
        public const string GUI = "GUI";
        /// <summary>
        /// Name of tag 'Hitbox'.
        /// </summary>
        public const string Hitbox = "Hitbox";
        /// <summary>
        /// Name of tag 'New Tag'.
        /// </summary>
        public const string New_Tag = "New Tag";
        /// <summary>
        /// Name of tag 'New Tag 2'.
        /// </summary>
        public const string New_Tag_2 = "New Tag 2";
    }

    public static class SortingLayers
    {
        /// <summary>
        /// ID of sorting layer 'Default'.
        /// </summary>
        public const int Default = 0;
        /// <summary>
        /// ID of sorting layer 'VFX'.
        /// </summary>
        public const int VFX = 638942701;
    }

    public static class Layers
    {
        /// <summary>
        /// Index of layer 'Default'.
        /// </summary>
        public const int Default = 0;
        /// <summary>
        /// Index of layer 'TransparentFX'.
        /// </summary>
        public const int TransparentFX = 1;
        /// <summary>
        /// Index of layer 'Ignore Raycast'.
        /// </summary>
        public const int Ignore_Raycast = 2;
        /// <summary>
        /// Index of layer 'Water'.
        /// </summary>
        public const int Water = 4;
        /// <summary>
        /// Index of layer 'UI'.
        /// </summary>
        public const int UI = 5;
        /// <summary>
        /// Index of layer 'Stage'.
        /// </summary>
        public const int Stage = 8;
        /// <summary>
        /// Index of layer 'Character'.
        /// </summary>
        public const int Character = 9;
        /// <summary>
        /// Index of layer 'Hitbox'.
        /// </summary>
        public const int Hitbox = 10;
        /// <summary>
        /// Index of layer 'Hurtbox'.
        /// </summary>
        public const int Hurtbox = 11;
        /// <summary>
        /// Index of layer 'Blast Zone'.
        /// </summary>
        public const int Blast_Zone = 31;

        /// <summary>
        /// Bitmask of layer 'Default'.
        /// </summary>
        public const int DefaultMask = 1 << 0;
        /// <summary>
        /// Bitmask of layer 'TransparentFX'.
        /// </summary>
        public const int TransparentFXMask = 1 << 1;
        /// <summary>
        /// Bitmask of layer 'Ignore Raycast'.
        /// </summary>
        public const int Ignore_RaycastMask = 1 << 2;
        /// <summary>
        /// Bitmask of layer 'Water'.
        /// </summary>
        public const int WaterMask = 1 << 4;
        /// <summary>
        /// Bitmask of layer 'UI'.
        /// </summary>
        public const int UIMask = 1 << 5;
        /// <summary>
        /// Bitmask of layer 'Stage'.
        /// </summary>
        public const int StageMask = 1 << 8;
        /// <summary>
        /// Bitmask of layer 'Character'.
        /// </summary>
        public const int CharacterMask = 1 << 9;
        /// <summary>
        /// Bitmask of layer 'Hitbox'.
        /// </summary>
        public const int HitboxMask = 1 << 10;
        /// <summary>
        /// Bitmask of layer 'Hurtbox'.
        /// </summary>
        public const int HurtboxMask = 1 << 11;
        /// <summary>
        /// Bitmask of layer 'Blast Zone'.
        /// </summary>
        public const int Blast_ZoneMask = 1 << 31;
    }

    public static class Scenes
    {
        /// <summary>
        /// ID of scene 'Game Start Bootstrap'.
        /// </summary>
        public const int Game_Start_Bootstrap = 0;
        /// <summary>
        /// ID of scene 'Splash Screen'.
        /// </summary>
        public const int Splash_Screen = 1;
        /// <summary>
        /// ID of scene 'Forest of Magic Stage'.
        /// </summary>
        public const int Forest_of_Magic_Stage = 2;
        /// <summary>
        /// ID of scene 'Physics Test Scene'.
        /// </summary>
        public const int Physics_Test_Scene = 3;
    }

    public static class Axes
    {
        /// <summary>
        /// Input axis 'Player 1 Horizontal'.
        /// </summary>
        public const string Player_1_Horizontal = "Player 1 Horizontal";
        /// <summary>
        /// Input axis 'Player 1 Vertical'.
        /// </summary>
        public const string Player_1_Vertical = "Player 1 Vertical";
        /// <summary>
        /// Input axis 'Player 1 Jump'.
        /// </summary>
        public const string Player_1_Jump = "Player 1 Jump";
        /// <summary>
        /// Input axis 'Player 1 Attack'.
        /// </summary>
        public const string Player_1_Attack = "Player 1 Attack";
        /// <summary>
        /// Input axis 'Player 1 Special'.
        /// </summary>
        public const string Player_1_Special = "Player 1 Special";
        /// <summary>
        /// Input axis 'Player 1 Shield'.
        /// </summary>
        public const string Player_1_Shield = "Player 1 Shield";
        /// <summary>
        /// Input axis 'Player 2 Horizontal'.
        /// </summary>
        public const string Player_2_Horizontal = "Player 2 Horizontal";
        /// <summary>
        /// Input axis 'Player 2 Vertical'.
        /// </summary>
        public const string Player_2_Vertical = "Player 2 Vertical";
        /// <summary>
        /// Input axis 'Player 2 Jump'.
        /// </summary>
        public const string Player_2_Jump = "Player 2 Jump";
        /// <summary>
        /// Input axis 'Player 2 Attack'.
        /// </summary>
        public const string Player_2_Attack = "Player 2 Attack";
        /// <summary>
        /// Input axis 'Player 2 Special'.
        /// </summary>
        public const string Player_2_Special = "Player 2 Special";
        /// <summary>
        /// Input axis 'Player 2 Shield'.
        /// </summary>
        public const string Player_2_Shield = "Player 2 Shield";
        /// <summary>
        /// Input axis 'Player 3 Horizontal'.
        /// </summary>
        public const string Player_3_Horizontal = "Player 3 Horizontal";
        /// <summary>
        /// Input axis 'Player 3 Vertical'.
        /// </summary>
        public const string Player_3_Vertical = "Player 3 Vertical";
        /// <summary>
        /// Input axis 'Player 3 Jump'.
        /// </summary>
        public const string Player_3_Jump = "Player 3 Jump";
        /// <summary>
        /// Input axis 'Player 3 Attack'.
        /// </summary>
        public const string Player_3_Attack = "Player 3 Attack";
        /// <summary>
        /// Input axis 'Player 3 Special'.
        /// </summary>
        public const string Player_3_Special = "Player 3 Special";
        /// <summary>
        /// Input axis 'Player 3 Shield'.
        /// </summary>
        public const string Player_3_Shield = "Player 3 Shield";
        /// <summary>
        /// Input axis 'Player 4 Horizontal'.
        /// </summary>
        public const string Player_4_Horizontal = "Player 4 Horizontal";
        /// <summary>
        /// Input axis 'Player 4 Vertical'.
        /// </summary>
        public const string Player_4_Vertical = "Player 4 Vertical";
        /// <summary>
        /// Input axis 'Player 4 Jump'.
        /// </summary>
        public const string Player_4_Jump = "Player 4 Jump";
        /// <summary>
        /// Input axis 'Player 4 Attack'.
        /// </summary>
        public const string Player_4_Attack = "Player 4 Attack";
        /// <summary>
        /// Input axis 'Player 4 Special'.
        /// </summary>
        public const string Player_4_Special = "Player 4 Special";
        /// <summary>
        /// Input axis 'Player 4 Shield'.
        /// </summary>
        public const string Player_4_Shield = "Player 4 Shield";
    }
}

