﻿using UnityEngine;
using System.Collections;

namespace Hourai {

    public interface IInputAxis {
        
        float GetAxisValue();

    }

}
