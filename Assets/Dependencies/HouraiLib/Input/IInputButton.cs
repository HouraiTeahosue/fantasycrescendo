﻿using UnityEngine;
using System.Collections;

namespace Hourai {


    public interface IInputButton {

        bool GetButtonValue();

    }

}
