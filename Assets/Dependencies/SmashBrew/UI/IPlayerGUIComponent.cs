﻿using UnityEngine;
using System.Collections;

namespace Hourai.SmashBrew.UI {

    public interface IPlayerGUIComponent {

        void SetPlayerData(Player data);

    }

}
